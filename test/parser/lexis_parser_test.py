import unittest
from lexis_parser import LexisParser, TextCleaner
from werkzeug.datastructures import FileStorage

class TestLexisParser(unittest.TestCase):

    def setUp(self):
        self.parser = LexisParser() #I could mock collaborators using fields, but I don't see much of a point
        # maybe if it gets big enough to require its own DI

    def test_can_get_list_of_citations_from_docx(self):
        testFile  = '../resources/test_doc.docx'
        self.parser.parseDocument(testFile)

        self.assertEqual(self.parser.getCitations(), ['443 U.S. 307, 319 (1979)'])

    def test_can_get_citations_from_pdf(self):
        testFile = '../resources/test_pdf.pdf'
        self.parser.parseDocument(testFile)

        self.assertTrue('50 Ohio St.2d 317, 327 (1977)' in self.parser.getCitations())

    def test_cleaner_removes_characters(self):
        testString = "Hello\n\t   <br /><h1>world!</h1>"
        cleaner = TextCleaner()

        self.assertEqual("Hello world!", cleaner.clean(testString))


if __name__ == '__main__':
    unittest.main()
