from unittest import TestCase
from readers.docx_reader import DOCXReader
from readers.pdf_reader import PDFReader
from lexis_parser import FileClassifier

class TestFileClassifier(TestCase):

    def setUp(self):
        pass

    def test_classifies_docx(self):
        testPath = 'fileName.docx'
        classifier = FileClassifier()

        reader = classifier.classifyFile(testPath)

        self.assertTrue(isinstance(reader, DOCXReader))

    def test_classifies_pdf(self):
        testPath = 'fileName.pdf'
        classifier = FileClassifier()

        reader = classifier.classifyFile(testPath)

        self.assertTrue(isinstance(reader, PDFReader))