import unittest
import flask_lexis
import io, zipfile, os
from io import StringIO, BytesIO


class TestLexisFlask(unittest.TestCase):

    def setUp(self):
        self.citation_name = "citationName"
        self.citation_url = "citationURL"
        self.citation_file = 'testURL'
        self.citation_history_entry = '<a class="citationLink" href="" onclick="return false;">%s</a>'
        flask_lexis.app.config['TESTING'] = True
        self.app = flask_lexis.app.test_client()
        self.testFileName = "../resources/test_doc.docx"

    def test_has_file_upload(self):
        resp = self.app.get('/')

        assert b'enctype="multipart/form-data"' in resp.get_data()
        assert b'No file submitted' in resp.get_data()
        assert b'No file history' in resp.get_data()

    def test_try_upload_without_file(self):
        resp = self.app.post(
            '/',
            data={'file': (BytesIO(b''), ''),}
        )

        assert b'No file submitted' in resp.get_data()
        assert b'No file history' in resp.get_data()

    def test_try_upload_with_file(self):
        file = open(self.testFileName, 'rb')
        resp = self.app.post(
            '/',
            data=self.get_request(file)
        )

        assert str.encode('Found citations in %s' % self.testFileName) in resp.get_data()


    def test_add_to_citation_history(self):
        file = open(self.testFileName, 'rb')
        filename = os.path.basename(file.name)
        resp = self.app.post(
            '/',
            data=self.get_request(file)
        )

        with self.app.session_transaction() as sess:
             self.assertTrue(filename in sess['history'])

    def test_select_citation_history(self):
        self.setup_session()

        resp = self.app.get(
            '/{}'.format(self.citation_file)
        )

        assert str.encode('Found citations in %s' % self.citation_file) in resp.get_data()
        assert str.encode('<a href="%s" target="_blank">%s</a>' % (self.citation_url, self.citation_name)) in resp.get_data()

    def test_delete_citation_from_history(self):
        self.setup_session()

        resp = self.app.delete(
            '/{}'.format(self.citation_file)
        )

        assert str.encode(self.citation_history_entry % self.citation_file) not in resp.get_data()


    def setup_session(self):
        with self.app.session_transaction() as sess:
            sess['history'] = {self.citation_file: [{"url": self.citation_url, "name": self.citation_name}]}

    def get_request(self, file):
        return {'file': (BytesIO(file.read()), file.name),}


