from flask import Flask, request, url_for, redirect, render_template, session
from lexis_parser import LexisParser
from werkzeug.wrappers import Response
import urllib.parse, os

app = Flask(__name__)
app.secret_key = 'This is really unique and secret'

@app.route('/', methods=['POST', 'GET'])
def uploadFile():
    initSession()

    file = request.files.get('file')
    citations = []

    if not file or not file.filename:
        headerText = 'No file submitted'
    else:
        tempDir = "%s/tmp" % os.path.expanduser("~")
        if not os.path.exists(tempDir):
            os.mkdir(tempDir)
        filePath = "%s/tmp/%s" % (os.path.expanduser("~") ,os.path.basename(file.filename))
        file.save(filePath)
        parser = LexisParser()
        parser.parseDocument(filePath)
        headerText = 'Found citations in {}'.format(file.filename)

        os.remove(filePath)

        for citation in parser.getCitations():
            citationURL = createCitationURL(citation)
            citations.append(dict(url=citationURL, name=citation))

        session.get('history')[os.path.basename(file.filename)] = citations

    return render_template('layout.html', headerText=headerText, citations=citations)

@app.route('/<fileName>', methods=['GET'])
def selectFromHistory(fileName):
    return render_template('show_citations.html',
                           headerText='Found citations in {}'.format(fileName),
                           citations=session.get('history').get(fileName))

@app.route('/<fileName>', methods=['DELETE'])
def removeFromHistory(fileName):
    session.get('history').pop(fileName, None)
    return render_template('show_citations.html',
                       headerText='Deleted citations from {}'.format(fileName),
                       citations={})


def initSession():
    if session.get('history') is None:
        session['history'] = {}

def createCitationURL(citation):
    baseURL = '''http://advance.lexis.com/api/document/citation?cite='''
    return baseURL+urllib.parse.quote(citation)

if __name__ == '__main__':
    app.run()