import pydocx

class DOCXReader(object):

    def __init__(self):
        pass

    def read_text(self, filePath):
        return pydocx.PyDocX.to_html(filePath)

