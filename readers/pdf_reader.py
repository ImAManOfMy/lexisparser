from subprocess import call
import PyPDF2, os

class PDFReader(object):

    def __init__(self):
        pass

    def read_text(self, filePath):
        call(["pypdfocr", filePath])

        ocrPDFFilePath = "%s_ocr%s" % (filePath[:-4], filePath[-4:])

        pdfReader = PyPDF2.PdfFileReader(open(ocrPDFFilePath, "rb"))

        resultText = ""
        for page in pdfReader.pages:
            resultText += page.extractText().replace('\n', " ")
        #TODO: implement a preprocessing class to strip garbage and remove newlines and the like

        os.remove(ocrPDFFilePath)

        return resultText