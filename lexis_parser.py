import re, sys
from html.parser import HTMLParser
from readers.docx_reader import DOCXReader
from readers.pdf_reader import PDFReader

class LexisParser(object):

    def __init__(self):
        self.citations = []
        self.html = ''
        self.classifier = FileClassifier()

    def parseDocument(self, filePath):
        reader = self.classifier.classifyFile(filePath)
        self.html = reader.read_text(filePath)

        lexisRegex = re.compile('\s((?:\d+)\s(?:.{,30}?)\s(?:(?:\d+),?\s?-?)+\s(?:\(.{,30}?\d{4}\)){1})')
        self.citations = lexisRegex.findall(TextCleaner().clean(self.html))

    def getCitations(self) -> [str]:
        return self.citations


class FileClassifier(object):

    def __init__(self):
        pass

    def classifyFile(self, filePath: str):
        if filePath.endswith(".docx"):
            return DOCXReader()
        elif filePath.endswith(".pdf"):
            return PDFReader()

class TextCleaner(object):

    def __init__(self):
        pass

    def clean(self, dirty_text: str):
        result = " ".join(dirty_text.split())
        result = self.strip_tags(result)

        return result

    def strip_tags(self, html: str):
        s = MLStripper()
        s.feed(html)
        return s.get_data()

# Totally got this from stackoverflow. Should probably test it directly, try to understand what it's actually doing
class MLStripper(HTMLParser):
    def __init__(self):
        super().__init__()
        self.reset()
        self.fed = []

    def handle_data(self, d):
        self.fed.append(d)

    def get_data(self):
        return ''.join(self.fed)




if __name__ == '__main__':
    parser = LexisParser()
    parser.parseDocument(sys.argv[1])
    print(parser.getCitations())